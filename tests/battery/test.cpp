// System Includes
#include <iostream>
#include <gtest/gtest.h>

// Project Includes
#include "battery_class.h"

// Namespace specifications
using std::cout;
using std::endl;

// Constants
const double seconds_per_hour = 3600;

// GetCapacityMax tests
TEST(GetCapacityMax, ReturnValueMatch) {
    // Local Variables
    double charge_max         =  320.0;
    double time_to_charge_max =  0.6; 

    Battery battery(charge_max, time_to_charge_max);
 
    EXPECT_EQ(charge_max, battery.GetCapacityMax());
}

// GetTimeToChargeMax tests
TEST(GetTimeToChargeMax, ReturnValueMatch) {
    // Local Variables
    double charge_max         =  320.0;
    double time_to_charge_max =  0.6; 

    // Construct the battery 
    Battery battery(charge_max, time_to_charge_max);

    // Test
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

// Charge tests
TEST(Charge, FullyCharged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6; 
    double time_step          = 1.0;

    // Construct the battery
    Battery battery(charge_max,time_to_charge_max);

    // Test
    EXPECT_EQ(0.0, battery.Charge(time_step));
    EXPECT_EQ(charge_max,battery.GetCapacity());

    EXPECT_EQ(charge_max, battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

TEST(Charge, PartiallyCharged) {
    // Local Variables
    double charge_max                    = 320.0;
    double time_to_charge_max            = 0.6; 
    double time_step                     = 1.0;
    double expected_charge_per_time_step = charge_max / (time_to_charge_max * seconds_per_hour);
    double discharge_kwh                 = 100.0;

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Discharge the battery
    battery.Discharge(discharge_kwh);

    // Test
    EXPECT_EQ(expected_charge_per_time_step, battery.Charge(time_step));
    EXPECT_EQ(charge_max - discharge_kwh + expected_charge_per_time_step, battery.GetCapacity());

    EXPECT_EQ(charge_max, battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

TEST(Charge, FullyDischarged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6; 
    double time_step          = 1.0;
    double expected_charge_per_time_step = charge_max / (time_to_charge_max * seconds_per_hour);

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Fully discharge the battery
    battery.Discharge(charge_max);

    // Test
    EXPECT_EQ(expected_charge_per_time_step, battery.Charge(time_step));
    EXPECT_EQ(expected_charge_per_time_step * 1, battery.GetCapacity());

    EXPECT_EQ(expected_charge_per_time_step, battery.Charge(time_step));
    EXPECT_EQ(expected_charge_per_time_step * 2, battery.GetCapacity());

    EXPECT_EQ(expected_charge_per_time_step, battery.Charge(time_step));
    EXPECT_EQ(expected_charge_per_time_step * 3, battery.GetCapacity());

    EXPECT_EQ(charge_max,  battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

// Discharge tests
TEST(Discharge, FullyCharged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6;
    double discharge_kwh      = 100.0; 

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Test
    EXPECT_EQ(discharge_kwh, battery.Discharge(discharge_kwh));
    EXPECT_EQ(charge_max - discharge_kwh * 1, battery.GetCapacity());

    EXPECT_EQ(charge_max,  battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

TEST(Discharge, PartiallyCharged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6;
    double discharge_kwh      = 100.0; 

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Test
    EXPECT_EQ(discharge_kwh, battery.Discharge(discharge_kwh));
    EXPECT_EQ(charge_max - discharge_kwh * 1, battery.GetCapacity());

    EXPECT_EQ(discharge_kwh, battery.Discharge(discharge_kwh));
    EXPECT_EQ(charge_max - discharge_kwh * 2, battery.GetCapacity());

    EXPECT_EQ(discharge_kwh, battery.Discharge(discharge_kwh));
    EXPECT_EQ(charge_max - discharge_kwh * 3, battery.GetCapacity());

    EXPECT_EQ(charge_max,  battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

TEST(Discharge, FullyDischarged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6;
    double discharge_kwh      = 100.0; 

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Fully discharge the battery
    battery.Discharge(charge_max);

    // Test
    EXPECT_EQ(0.0, battery.Discharge(discharge_kwh));
    EXPECT_EQ(0.0, battery.GetCapacity());

    EXPECT_EQ(charge_max,battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

// IsFullyCharged tests
TEST(IsFullyCharged, FullyCharged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6; 

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Test
    EXPECT_EQ(true, battery.IsFullyCharged());

    EXPECT_EQ(charge_max,battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

TEST(IsFullyCharged, PartiallyCharged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6;
    double discharge_kwh      = 100.0;

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Discharge the battery for one time step
    battery.Discharge(discharge_kwh);

    // Test
    EXPECT_EQ(false, battery.IsFullyCharged());

    EXPECT_EQ(charge_max,battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

TEST(IsFullyCharged, FullyDischarged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6; 

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Fully discharge the battery
    battery.Discharge(charge_max);

    // Test
    EXPECT_EQ(false, battery.IsFullyCharged());

    EXPECT_EQ(charge_max,battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

// IsCharging tests
TEST(IsCharging, FullyCharged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6; 

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Test
    EXPECT_EQ(false, battery.IsCharging());

    EXPECT_EQ(charge_max,battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

TEST(IsCharging, PartiallyCharged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6;
    double discharge_kwh      = 100.0;
    double time_step          = 1.0;

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Discharge the battery for one time step
    battery.Discharge(discharge_kwh);

    // Test
    EXPECT_EQ(false, battery.IsCharging());

    battery.Charge(time_step);

    EXPECT_EQ(true, battery.IsCharging());

    EXPECT_EQ(charge_max,battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

TEST(IsCharging, FullyDischarged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6;
    double time_step          = 1.0;

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Fully discharge the battery
    battery.Discharge(charge_max);

    // Test
    EXPECT_EQ(false, battery.IsCharging());

    battery.Charge(time_step);

    EXPECT_EQ(true, battery.IsCharging());

    EXPECT_EQ(charge_max,battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

// GetCapacity tests
TEST(GetCapacity, FullyCharged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6; 

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Test
    EXPECT_EQ(charge_max, battery.GetCapacity());

    EXPECT_EQ(charge_max,battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

TEST(GetCapacity, PartiallyCharged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6;
    double discharge_kwh      = 100.0;

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Discharge the battery for one time step
    battery.Discharge(discharge_kwh);

    // Test
    EXPECT_EQ(charge_max - discharge_kwh, battery.GetCapacity());

    EXPECT_EQ(charge_max,battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

TEST(GetCapacity, FullyDischarged) {
    // Local Variables
    double charge_max         = 320.0;
    double time_to_charge_max = 0.6; 

    // Construct the battery
    Battery battery(charge_max, time_to_charge_max);

    // Fully discharge the battery
    battery.Discharge(charge_max);

    // Test
    EXPECT_EQ(0.0, battery.GetCapacity());

    EXPECT_EQ(charge_max,battery.GetCapacityMax());
    EXPECT_EQ(time_to_charge_max, battery.GetTimeToChargeMax());
}

