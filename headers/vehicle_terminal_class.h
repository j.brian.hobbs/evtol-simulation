#ifndef VEHICLE_TERMINAL_CLASS_H
#define VEHICLE_TERMINAL_CLASS_H

// System Includes
#include <vector>

// Project Includes
#include "vehicle_class.h"

// Namespace specifications
using std::vector;

class VehicleTerminal {
    private:
        vector<Vehicle*> charging_stations_;
        vector<Vehicle*> landing_pads_;

        void ConnectToAvailableChargingStation();
        void DisconnectFromChargingStation(Vehicle*);

    public:
             VehicleTerminal(uint8_t num_charging_stations = 3);   // Total number of charging stations.

        bool AddToLandingPad(Vehicle*);                              // Returns if terminal was able to add the vehicle to a landing pad
        bool RemoveFromLandingPad(Vehicle*);                         // Returns if terminal was able to remove the vehicle from a landing pad
};

#endif