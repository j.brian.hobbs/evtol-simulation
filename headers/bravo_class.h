#ifndef BRAVO_CLASS_H
#define BRAVO_CLASS_H

// Project Includes
#include "vehicle_class.h"

class Bravo : public Vehicle {
    public:
        Bravo();
        Bravo(const Bravo&);
       ~Bravo();   
};

#endif