#ifndef DATA_STRUCTS_H
#define DATA_STRUCTS_H

// System Includes
#include <vector>

// Enumerations
enum class VehicleType {
  alpha = 0,
  bravo,
  charlie,
  delta,
  echo,
  size
};

// Structs
struct FlightData {
    double   time_inair_;           // Hours per flight

    double   dist_traveled_;        // Miles per flight
    double   dist_passengers_;      // Miles per flight

    uint32_t faults_count_;         // Counts per flight
    uint32_t passenger_count_;      // Counts per flight
};

struct ChargingData {
    double   time_charging_;        // Hours per charge
};

struct VehicleData {
    std::vector<FlightData>   flights_; // Data per flight
    std::vector<ChargingData> charges_; // Data per charging session
};

#endif