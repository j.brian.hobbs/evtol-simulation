#ifndef ALPHA_CLASS_H
#define ALPHA_CLASS_H

// Project Includes
#include "vehicle_class.h"

class Alpha : public Vehicle {
    public:
        Alpha();
        Alpha(const Alpha&);
       ~Alpha();        
};

#endif