#ifndef CHARLIE_CLASS_H
#define CHARLIE_CLASS_H

// Project Includes
#include "vehicle_class.h"

class Charlie : public Vehicle {
    public:
        Charlie();
        Charlie(const Charlie&);
       ~Charlie();   
};

#endif