#ifndef FLIGHT_STATS_CLASS_H
#define FLIGHT_STATS_CLASS_H

// System Includes
#include <cstdint>
#include <iostream>
#include <map>

// Project Inclues
#include "data_structs.h"
#include "fleet_class.h"

// Namespace specifications
using std::ostream;

class StatsAccumulator {
    private:
        std::map<VehicleType, VehicleData> data_;                    // Map of statistics for each vehicle type

    public:                                                         
                 StatsAccumulator();                                 // Default constructor

        void     GetFleetData(const Fleet&);                         // Add the fleet data to the stat accumulator

        size_t   AddFlightData(VehicleType, FlightData);             // Returns the number of flight data for the vehicle type added
        size_t   AddChargeData(VehicleType, ChargingData);           // Returns the number of charging data for the vehicle type added
        size_t   AddVehicleData(VehicleType, VehicleData);           // Returns the number of flight and charging data for the vehicle type added

        size_t   GetFlightsCount(VehicleType);                       // Number of flights flown
        size_t   GetChargesCount(VehicleType);                       // Number of charging sessions
        double   GetAvgDistTraveled(VehicleType);                    // Miles
        double   GetAvgTimeFlight(VehicleType);                      // Hours
        double   GetAvgTimeCharging(VehicleType);                    // Hours
        uint32_t GetFaultsCount(VehicleType);                        // Counts
        double   GetPassengerMiles(VehicleType);                     // Miles

        ostream& PrintFlightData(ostream &out,                       // Write the stats for the specified flight number in comma seperated format
                                  VehicleType vehicle_type,          // The type of vehicle of interest
                                  size_t      flight_number,         // Inputing flight number 0 returns the stats for the last flight
                                  bool        pretty_print = false); // pretty_print = false: returns variables in comma seperated format, pretty_print = true: returns variables as "Variable Name: [Value]"

        ostream& PrintChargeData(ostream &out,                       // Write the stats for the specified charging session number
                                  VehicleType vehicle_type,          // The type of vehicle of interest
                                  size_t      charge_number,         // Inputing charge number 0 returns the stats for the last charging session
                                  bool        pretty_print = false); // pretty_print = false: returns variables in comma seperated format, pretty_print = true: returns variables as "Variable Name: [Value]"
             
        ostream& PrintFlightDataHeader(ostream&);                    // Write the flight stats variable's headers
        ostream& PrintChargeDataHeader(ostream&);                    // Write the charging session stats variable's headers

        friend ostream& operator<<(ostream&, StatsAccumulator&);
};

#endif