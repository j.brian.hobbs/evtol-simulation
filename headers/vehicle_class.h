#ifndef VEHICLE_CLASS_H
#define VEHICLE_CLASS_H

// Project Includes
#include "data_structs.h"
#include "battery_class.h"

class Vehicle {
    private:
        bool inair_;                       // intial state is true
        bool connected_to_charger_;        // intial state is false

        FlightData   flight_data_current_; // Data for the current flight
        ChargingData charge_data_current_; // Data for the current charging session
        VehicleData  vehicle_data_;        // Data for all completed flights and charging sessions

        void     Launch();
        void     Land();
        uint32_t GetFaults(double time_s) const;

        void ClearFlightData(FlightData& flight_data);
        void ClearChargingData(ChargingData& charging_data);

    protected:
        Battery* p_battery_;            // Pointer to the battery. This needs to be dynamically created by the child class

        double   speed_cruise_mph_;
        uint32_t passenger_count_max_;  // Max number of passengers per flight
        double   fault_probability_h_;  // Probabilty of a fault per hour 
        double   energy_cruise_rate_s_; // kWh used at cruising speed per second
 
             Vehicle();                 // Vehicle Class is abstact


    public:
        void TimeStep(double time_s);

        void ConnectToCharger();
        void DisconnectFromCharger();

        bool        GetInAir() const;
        bool        GetConnectedToCharger() const;

        VehicleData GetData() const;    // Gets the flight and charging data for the vehicle
};

#endif