#ifndef RANDOMIZER_CLASS_H
#define RANDOMIZER_CLASS_H

class Randomizer {
    private:
        unsigned int seed_;

    public:
               Randomizer(unsigned int seed = 0);
        int    GetRandomNumber(int min_value, int max_value);
        double GetRandomNumber(double min_value, double max_value);

};

#endif