#ifndef FLEET_CLASS_H
#define FLEET_CLASS_H

// System Includes
#include <vector>

// Project Includes
#include "data_structs.h"
#include "alpha_class.h"
#include "bravo_class.h"
#include "charlie_class.h"
#include "delta_class.h"
#include "echo_class.h"

// Namespace specifications
using std::vector;

class Fleet {
    private:

        vector<Alpha>   alpha_vehicles_;   // Vector of vehicles for each alpha type
        vector<Bravo>   bravo_vehicles_;   // Vector of vehicles for each bravo type
        vector<Charlie> charlie_vehicles_; // Vector of vehicles for each charlie type
        vector<Delta>   delta_vehicles_;   // Vector of vehicles for each delta type
        vector<Echo>    echo_vehicles_;    // Vector of vehicles for each echo type

        vector<VehicleType> GetExludedVehicleTypes(int8_t num_alphas,
                                                   int8_t num_bravos,
                                                   int8_t num_charlies,
                                                   int8_t num_deltas,
                                                   int8_t num_echos);

        vector<uint8_t>     CreateRandomVehicles(uint8_t number_to_construct,                // Returns a vector of the number of each vehicle type created
                                                 vector<VehicleType> exluded_vehicle_types);


    public:
             // Fleet will create vehicles of the specified type upto num_vehicles starting with the number alphas specified...
             // followed by the other vehicle types in the order listed.  If the accumulating total of vehicle types specified is ...
             // less than num_vehicles, Fleet will construct the remaining vehicles using random vehicle types.
             // Setting a specific vehicle type number to 0 will generate none of that type.
             // Setting a specific vehicle type to -1 may result in randomly constructed vehicles of that type.

             Fleet(uint8_t num_vehicles = 20,   // Total number of vehicles to construct.
                   int8_t  num_alphas   = -1,   // Total number of alphas to construct
                   int8_t  num_bravos   = -1,   // Total number of alphas to construct
                   int8_t  num_charlies = -1,   // Total number of alphas to construct
                   int8_t  num_deltas   = -1,   // Total number of alphas to construct
                   int8_t  num_echos    = -1);  // Total number of alphas to construct

        void TimeStep(double time_s);

        vector<VehicleData> GetData(VehicleType) const;  // Gets the flight and charging data for all the specified vehicle types
        vector<Vehicle*>    GetOnGroundVehicles();       // Gets a pointer too all the vehicles
};

#endif