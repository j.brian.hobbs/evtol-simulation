#ifndef ECHO_CLASS_H
#define ECHO_CLASS_H

// Project Includes
#include "vehicle_class.h"

class Echo : public Vehicle {
    public:
        Echo();
        Echo(const Echo&);
       ~Echo(); 
   
};

#endif