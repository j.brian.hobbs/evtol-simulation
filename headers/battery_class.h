
#ifndef BATTERY_CLASS_H
#define BATTERY_CLASS_H

class Battery {

    private:
        double battery_capacity_;       // kWh
        double battery_capacity_max_;   // kWh
        double time_to_charge_max_;     // hours
        bool   is_charging_;
        double charge_rate_per_second_;

        const double epsilson_;         // Tolerance used to determine if a battery is fully charged

    public:
        Battery(double battery_capacity_max_kwh, // Max battery capacity
                double time_to_charge_max_h);    // Time to fully charge battery

        double GetCapacityMax() const;
        double GetTimeToChargeMax() const;
                                               
        double Charge(double time_s);            // Returns the amount of energy charged
        double Discharge(double energy_kwh);     // Returns the amount of energy discharged
        
        bool   IsFullyCharged() const;
        bool   IsCharging() const;
        double GetCapacity() const;
};

#endif