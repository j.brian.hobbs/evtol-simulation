#ifndef DELTA_CLASS_H
#define DELTA_CLASS_H

// Project Includes
#include "vehicle_class.h"

class Delta : public Vehicle {
    public:
        Delta();
        Delta(const Delta&);
       ~Delta(); 
};

#endif