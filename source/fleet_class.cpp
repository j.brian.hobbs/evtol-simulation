// System Includes
#include <algorithm>

// Project Includes
#include "randomizer_class.h"
#include "fleet_class.h"
#include "alpha_class.h"
#include "bravo_class.h"
#include "charlie_class.h"
#include "delta_class.h"
#include "echo_class.h"

// Namespace specifications
using std::vector;
using std::min;

// Forward declare globals
extern Randomizer g_randomizer;

Fleet::Fleet(uint8_t num_vehicles,
             int8_t  num_alphas,
             int8_t  num_bravos,
             int8_t  num_charlies,
             int8_t  num_deltas,
             int8_t  num_echos) : alpha_vehicles_(),
                                  bravo_vehicles_(),
                                  charlie_vehicles_(),
                                  delta_vehicles_(),
                                  echo_vehicles_()
{
    // TODO: reduce the repetative code by using function pointers to the constructors
    
    // Local variables
    uint8_t             remaining_to_construct = num_vehicles;
    vector<VehicleType> exluded_vehicle_types;

    // Add Alphas
    if(num_alphas > 0)
    {
        // Clip the number constucted to the number remaining to constructed
        num_alphas = min((uint8_t)num_alphas, remaining_to_construct);

        // Constuct the vechiles and add them to the fleet
        for(uint8_t i = 0; i < num_alphas; ++i) {
            alpha_vehicles_.push_back(Alpha());
        }
 
        remaining_to_construct -= num_alphas;
    }

    // Add Bravos
    if(remaining_to_construct > 0 && num_bravos > 0)
    {
        // Clip the number constucted to the number remaining to constructed
        num_bravos = min((uint8_t)num_bravos, remaining_to_construct);

        // Constuct the vechiles and add them to the fleet
        for(uint8_t i = 0; i < num_bravos; ++i) {
            bravo_vehicles_.push_back(Bravo());
        }
 
        remaining_to_construct -= num_bravos;
    }

    // Add Charlies
    if(remaining_to_construct > 0 && num_charlies > 0)
    {
        // Clip the number constucted to the number remaining to constructed
        num_charlies = min((uint8_t)num_charlies, remaining_to_construct);

        // Constuct the vechiles and add them to the fleet
        for(uint8_t i = 0; i < num_charlies; ++i) {
            charlie_vehicles_.push_back(Charlie());
        }
 
        remaining_to_construct -= num_charlies;
    }

    // Add Deltas
    if(remaining_to_construct > 0 && num_deltas > 0)
    {
        // Clip the number constucted to the number remaining to constructed
        num_deltas = min((uint8_t)num_deltas, remaining_to_construct);

        // Constuct the vechiles and add them to the fleet
        for(uint8_t i = 0; i < num_deltas; ++i) {
            delta_vehicles_.push_back(Delta());
        }
 
        remaining_to_construct -= num_deltas;
    }

    // Add Echos
    if(remaining_to_construct > 0 && num_echos > 0)
    {
        // Clip the number constucted to the number remaining to constructed
        num_echos = min((uint8_t)num_echos, remaining_to_construct);

        // Constuct the vechiles and add them to the fleet
        for(uint8_t i = 0; i < num_echos; ++i) {
            echo_vehicles_.push_back(Echo());
        }
 
        remaining_to_construct -= num_echos;
    }

    if(remaining_to_construct > 0)
    {
        exluded_vehicle_types = GetExludedVehicleTypes(num_alphas, num_bravos, num_charlies, num_deltas, num_echos);

        CreateRandomVehicles(remaining_to_construct, exluded_vehicle_types);
    }
}

vector<VehicleType> Fleet::GetExludedVehicleTypes(int8_t num_alphas,
                                                  int8_t num_bravos,
                                                  int8_t num_charlies,
                                                  int8_t num_deltas,
                                                  int8_t num_echos) {
    // Local varibles
    vector<VehicleType> exluded_vehicle_types;

    // Create a vector of the exluded vehicle types
    if(num_alphas == 0) {
        exluded_vehicle_types.push_back(VehicleType::alpha);
    }

    if(num_bravos == 0) {
        exluded_vehicle_types.push_back(VehicleType::bravo);
    }

    if(num_charlies == 0) {
        exluded_vehicle_types.push_back(VehicleType::charlie);
    }

    if(num_deltas == 0) {
        exluded_vehicle_types.push_back(VehicleType::delta);
    }

    if(num_echos == 0) {
        exluded_vehicle_types.push_back(VehicleType::echo);
    }

    // If all vehicle types are exluded create alphas
    // TODO: replace this with handling of no vehicles
    if(exluded_vehicle_types.size() == (unsigned int)VehicleType::size) {
        exluded_vehicle_types.erase(exluded_vehicle_types.begin());
    }

    return exluded_vehicle_types;
}

vector<uint8_t> Fleet::CreateRandomVehicles(uint8_t             number_to_construct,
                                            vector<VehicleType> exluded_vehicle_types) {
    // Local variables
    uint8_t      remaining_to_construct    = number_to_construct;
    unsigned int vehicle_type_index = 0;

    vector<uint8_t>      num_created((unsigned int)VehicleType::size);
    unsigned int         vehicle_type = (unsigned int)VehicleType::alpha;
    vector<unsigned int> vehicle_types;                                   // This is really a VehicleType, but making it an unsigned int to reduce the amount of casting needed

    // Randomly select the order of vehicle types to create
    for(unsigned int i = 0; i < (unsigned int)VehicleType::size - exluded_vehicle_types.size();) {

        // Get a random vehicle type to construct
        vehicle_type = g_randomizer.GetRandomNumber(0, (unsigned int)VehicleType::size - 1);

        // Check if the vehicle type should be exluded
        if(std::find(exluded_vehicle_types.begin(), exluded_vehicle_types.end(), (VehicleType)vehicle_type) != exluded_vehicle_types.end()) {
            // Get another vehicle type
            continue;
        }

        // Get a unique vehicle type to add to the list
        if(std::find(vehicle_types.begin(), vehicle_types.end(), vehicle_type) == vehicle_types.end())
        {
            vehicle_types.push_back(vehicle_type);
            ++i;
        }
    }

    // Randomly create vehicles of each type until the total number of vehicles are created
    while(remaining_to_construct > 0)
    {
        // Constuct a random number of the next vehicle type
        number_to_construct = g_randomizer.GetRandomNumber(0, (unsigned int)remaining_to_construct);

        // TODO Simplify to reduce code here
        switch(vehicle_types.at(vehicle_type_index))
        {
            case (unsigned int)VehicleType::alpha:
                // Constuct the vechiles and add them to the fleets
                for(uint8_t i = 0; i < number_to_construct; ++i) {
                    alpha_vehicles_.push_back(Alpha());
                }
                break;

            case (unsigned int)VehicleType::bravo:
                // Constuct the vechiles and add them to the fleet
                for(uint8_t i = 0; i < number_to_construct; ++i) {
                    bravo_vehicles_.push_back(Bravo());
                }
                break;

            case (unsigned int)VehicleType::charlie:
                // Constuct the vechiles and add them to the fleet
                for(uint8_t i = 0; i < number_to_construct; ++i) {
                    charlie_vehicles_.push_back(Charlie());
                }
                break;

            case (unsigned int)VehicleType::delta:
                // Constuct the vechiles and add them to the fleet
                for(uint8_t i = 0; i < number_to_construct; ++i) {
                    delta_vehicles_.push_back(Delta());
                }
                break;

            case (unsigned int)VehicleType::echo:
                // Constuct the vechiles and add them to the fleet
                for(uint8_t i = 0; i < number_to_construct; ++i) {
                    echo_vehicles_.push_back(Echo());
                }
                break;
        }

        // Reduce the number left to construct
        remaining_to_construct -= number_to_construct;
    
        // Record the number of vehicles constructed
        num_created.at(vehicle_types.at(vehicle_type_index)) += number_to_construct;

        // Get the next vehicle type to construct
        if(++vehicle_type_index >= (unsigned int)vehicle_types.size()) {
            vehicle_type_index = 0;
        }
    }

    return num_created;
}

void Fleet::TimeStep(double time_s) {

    // Call TimeStep for all alpha vehicles
    for(size_t i = 0; i < alpha_vehicles_.size(); ++i) {
        alpha_vehicles_.at(i).TimeStep(time_s);
    }

    // Call TimeStep for all bravo vehicles
    for(size_t i = 0; i < bravo_vehicles_.size(); ++i) {
        bravo_vehicles_.at(i).TimeStep(time_s);
    }

    // Call TimeStep for all charlie vehicles
    for(size_t i = 0; i < charlie_vehicles_.size(); ++i) {
        charlie_vehicles_.at(i).TimeStep(time_s);
    }

    // Call TimeStep for all delta vehicles
    for(size_t i = 0; i < delta_vehicles_.size(); ++i) {
        delta_vehicles_.at(i).TimeStep(time_s);
    }

    // Call TimeStep for all echo vehicles
    for(size_t i = 0; i < echo_vehicles_.size(); ++i) {
        echo_vehicles_.at(i).TimeStep(time_s);
    } 
}

vector<VehicleData> Fleet::GetData(VehicleType vehicle_type) const {
 // Local varibles
vector<VehicleData> vehicle_data_set;

    // Get all the vehicle data for all alphas
    if(vehicle_type == VehicleType::alpha) {

        for(size_t i = 0; i < alpha_vehicles_.size(); ++i) {
            vehicle_data_set.push_back(alpha_vehicles_.at(i).GetData());
        }
    }

    // Get all the vehicle data for all bravos
    if(vehicle_type == VehicleType::bravo) {

        for(size_t i = 0; i < bravo_vehicles_.size(); ++i) {
            vehicle_data_set.push_back(bravo_vehicles_.at(i).GetData());
        }
    }

    // Get all the vehicle data for all charlies
    if(vehicle_type == VehicleType::charlie) {

        for(size_t i = 0; i < charlie_vehicles_.size(); ++i) {
            vehicle_data_set.push_back(charlie_vehicles_.at(i).GetData());
        }
    }

    // Get all the vehicle data for all deltas
    if(vehicle_type == VehicleType::delta) {

        for(size_t i = 0; i < delta_vehicles_.size(); ++i) {
            vehicle_data_set.push_back(delta_vehicles_.at(i).GetData());
        }
    }

    // Get all the vehicle data for all echos
    if(vehicle_type == VehicleType::echo) {

        for(size_t i = 0; i < echo_vehicles_.size(); ++i) {
            vehicle_data_set.push_back(echo_vehicles_.at(i).GetData());
        }
    }

    return vehicle_data_set;
}

vector<Vehicle*> Fleet::GetOnGroundVehicles() {
    // Local Variables
    vector<Vehicle*> p_fleet;

    // Get pointers to all the vehicles in the fleet
    for(size_t i = 0; i < alpha_vehicles_.size(); ++i) {
        if(alpha_vehicles_.at(i).GetInAir() == false) {
            p_fleet.push_back((Vehicle*)&alpha_vehicles_.at(i));
        }
    }

    for(size_t i = 0; i < bravo_vehicles_.size(); ++i) {
        if(bravo_vehicles_.at(i).GetInAir() == false) {
            p_fleet.push_back((Vehicle*)&bravo_vehicles_.at(i));
        } 
    }

    for(size_t i = 0; i < charlie_vehicles_.size(); ++i) {
        if(charlie_vehicles_.at(i).GetInAir() == false) {
            p_fleet.push_back((Vehicle*)&charlie_vehicles_.at(i));
        }
    }

    for(size_t i = 0; i < delta_vehicles_.size(); ++i) {
        if(delta_vehicles_.at(i).GetInAir() == false) {
            p_fleet.push_back((Vehicle*)&delta_vehicles_.at(i));
        }
    }

    for(size_t i = 0; i < echo_vehicles_.size(); ++i) {
        if(echo_vehicles_.at(i).GetInAir() == false) {
            p_fleet.push_back((Vehicle*)&echo_vehicles_.at(i));
        }
    }

    return p_fleet;
}
