// Project Includes
#include "vehicle_class.h"
#include "randomizer_class.h"
#include "vehicle_terminal_class.h"

// Forward declare globals
extern Randomizer g_randomizer;
extern VehicleTerminal* g_p_terminal; // TODO get this through an object

Vehicle::Vehicle()
{
    inair_     = true;  // Start the vehicle in the air since it is fully charged and will instantanously starting cruisiing.
                        // This elimantes the issue of pushing the intial charge session data set with 0 charging time to the stats

    connected_to_charger_ = false;

    flight_data_current_ = {};
    charge_data_current_ = {};

    p_battery_ = NULL;

    speed_cruise_mph_      = 0.0;
    passenger_count_max_   = 0;
    fault_probability_h_   = 0.0;
    energy_cruise_rate_s_  = 0.0;  
}

void Vehicle::ClearFlightData(FlightData& flight_data) {
    // Clear all the data
    flight_data.time_inair_      = 0.0;
    flight_data.dist_traveled_   = 0.0;
    flight_data.dist_passengers_ = 0.0;
    flight_data.faults_count_    = 0;
    flight_data.passenger_count_ = 0;
}

void Vehicle::ClearChargingData(ChargingData& charging_data) {
    // Clear all the data
    charging_data.time_charging_ = 0.0;
}

void Vehicle::Launch() {
    
    // Record the current charge data
    vehicle_data_.charges_.push_back(charge_data_current_);

    // Clear the current charging data
    ClearChargingData(charge_data_current_);
    
    // Remove from the vehicle from the landing pad
    g_p_terminal->RemoveFromLandingPad(this);

    // Flag the vehicle as in air
    inair_ = true;
}

void Vehicle::Land() {

    // Record the current flight data
    flight_data_current_.passenger_count_ = passenger_count_max_;
    flight_data_current_.dist_passengers_ = flight_data_current_.dist_traveled_ * flight_data_current_.passenger_count_;

    vehicle_data_.flights_.push_back(flight_data_current_);

    // Clear the current flight data
    ClearFlightData(flight_data_current_);

    // Add the vehicle to the landing pad
    g_p_terminal->AddToLandingPad(this);

    // Flag the vehicle as on the ground
    inair_ = false;
    
}

uint32_t Vehicle::GetFaults(double time_s) const {
    // Local variables
    const uint32_t seconds_per_hour = 3600; // TODO create a constants file

    uint32_t faults_count           = 0;

    double fault_probability_s = fault_probability_h_ / seconds_per_hour;
    double random_value = g_randomizer.GetRandomNumber(0.0, 1.0);
    
    // Determine if there is a fault in this time span
    if(random_value <= fault_probability_s * time_s){
        faults_count = 1; // TODO: this assumes there can only be one fault per call the GetFaults.  Consider redesigning to allow multiple faults per call
    }
    
    return faults_count;
}

void Vehicle::TimeStep(double time_s) {
    // Local variables
    const uint32_t seconds_per_hour = 3600; // TODO create a constants files 

    // Vehicle is on the ground
    if(inair_ == false) {

        // Check if connected to charging station
        if(connected_to_charger_ == true)
        {
            // Charge the vehicle if it is connected to a charger
            p_battery_->Charge(time_s);
            charge_data_current_.time_charging_ += time_s;

            // Launch the vehicle if the battery is full
            if(p_battery_->IsFullyCharged() == true) {
                this->Launch();
            }
        }
    }
    // Vehicle is in the air
    else {
        // Discharge the battery
        p_battery_->Discharge(energy_cruise_rate_s_ * time_s);

        // Check for faults
        flight_data_current_.faults_count_ += GetFaults(time_s);

        // Record data
        flight_data_current_.time_inair_    += time_s;
        flight_data_current_.dist_traveled_ += speed_cruise_mph_ / seconds_per_hour * time_s;
        
        // Land the vehicle if the battery is depleated
        if (p_battery_->GetCapacity() == 0.0){
            this->Land();
        }
    }
}

void Vehicle::ConnectToCharger() {
    connected_to_charger_ = true;
}

void Vehicle::DisconnectFromCharger() {
    connected_to_charger_ = false;
};

bool Vehicle::GetInAir() const {
    return inair_; 
}

bool Vehicle::GetConnectedToCharger() const {
    return connected_to_charger_;
}

VehicleData Vehicle::GetData() const {
    return vehicle_data_;
}



