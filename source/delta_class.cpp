// Project Includes
#include "vehicle_class.h"
#include "delta_class.h"

Delta::Delta() {

    // TODO: move to configuration file
    const double   speed_cruise_mph               = 90.0;
    const double   battery_capacity_max_kwh       = 120.0;
    const double   time_to_charge_max_h           = 0.62;
    const double   energy_use_cruise_kwh_per_mile = 0.8;
    const uint32_t passenger_count_max            = 2;
    const double   fault_probability_h            = 0.22;

    // Local Variables
    const uint32_t seconds_per_hour = 3600; // TODO create a constants file

    p_battery_ = new Battery(battery_capacity_max_kwh, time_to_charge_max_h);

    speed_cruise_mph_ = speed_cruise_mph;
    passenger_count_max_ = passenger_count_max;
    fault_probability_h_ = fault_probability_h;

    // Calculate the amount of energy used cruising per second
    energy_cruise_rate_s_ =  energy_use_cruise_kwh_per_mile * speed_cruise_mph / seconds_per_hour;
}

Delta::Delta(const Delta& rhs_alpha) {

    p_battery_ = new Battery(rhs_alpha.p_battery_->GetCapacityMax(), rhs_alpha.p_battery_->GetTimeToChargeMax());

    speed_cruise_mph_    = rhs_alpha.speed_cruise_mph_;
    passenger_count_max_ = rhs_alpha.passenger_count_max_;
    fault_probability_h_ = rhs_alpha.fault_probability_h_;

    // Calculate the amount of energy used cruising per second
    energy_cruise_rate_s_ = rhs_alpha.energy_cruise_rate_s_;
}

Delta::~Delta()
{
    delete(p_battery_);
}