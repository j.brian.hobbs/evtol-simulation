// Project Includes
#include "vehicle_class.h"
#include "alpha_class.h"

Alpha::Alpha() {

    // TODO: move to configuration file
    const double   speed_cruise_mph               = 120.0;
    const double   battery_capacity_max_kwh       = 320.0;
    const double   time_to_charge_max_h           = 0.6;
    const double   energy_use_cruise_kwh_per_mile = 1.6;
    const uint32_t passenger_count_max            = 4;
    const double   fault_probability_h            = 0.25;

    // Local Variables
    const unsigned int seconds_per_hour = 3600; // TODO create a constants file

    p_battery_ = new Battery(battery_capacity_max_kwh, time_to_charge_max_h);

    speed_cruise_mph_    = speed_cruise_mph;
    passenger_count_max_ = passenger_count_max;
    fault_probability_h_ = fault_probability_h;

    // Calculate the amount of energy used cruising per second
    energy_cruise_rate_s_ =  energy_use_cruise_kwh_per_mile * speed_cruise_mph / seconds_per_hour;
}

Alpha::Alpha(const Alpha& rhs_alpha) {

    p_battery_ = new Battery(rhs_alpha.p_battery_->GetCapacityMax(), rhs_alpha.p_battery_->GetTimeToChargeMax());

    speed_cruise_mph_    = rhs_alpha.speed_cruise_mph_;
    passenger_count_max_ = rhs_alpha.passenger_count_max_;
    fault_probability_h_ = rhs_alpha.fault_probability_h_;

    // Calculate the amount of energy used cruising per second
    energy_cruise_rate_s_ = rhs_alpha.energy_cruise_rate_s_;
}

Alpha::~Alpha()
{
    delete(p_battery_);
}