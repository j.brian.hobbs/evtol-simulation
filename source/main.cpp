// System Includes
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <chrono>
#include <ctime>

// Project Includes
#include "randomizer_class.h"
#include "fleet_class.h"
#include "vehicle_terminal_class.h"
#include "stats_accumulator_class.h"

// Namespace specifications
using std::cout;
using std::endl;
using std::vector;

#include <iostream>
#include <fstream>

Randomizer g_randomizer(0); // TODO set the seed for the sim so we can reproduce results
VehicleTerminal* g_p_terminal(NULL); // TODO get this through an object

int main() {

    // Get the current clock time so we can measure how long the simulation runs for 
    auto clock_time_start = std::chrono::system_clock::now();
    
    // Initialize constants
    const unsigned int seconds_per_hour = 3600; // TODO create a constants file

    // Initialize simulation parameters
    const double       simulation_run_time_hrs = 3.0;                           // hours
    const double       time_delta              = 1.0;                           // seconds

    const uint8_t num_vehicles_total           = 20;                            // Total number of vehicles to construct
    const int8_t  num_alphas_min               = -1;                            // Minimum number of alphas to construct.   Number is clipped to num_vehicles_total. -1: no minimum. 0: create none
    const int8_t  num_bravos_min               = -1;                            // Minimum number of bravos to construct.   Number is clipped to num_vehicles_total. -1: no minimum. 0: create none
    const int8_t  num_charles_min              = -1;                            // Minimum number of charlies to construct. Number is clipped to num_vehicles_total. -1: no minimum. 0: create none
    const int8_t  num_deltas_min               = -1;                            // Minimum number of deltas to construct.   Number is clipped to num_vehicles_total. -1: no minimum. 0: create none
    const int8_t  num_echos_min                = -1;                            // Minimum number of echos to construct.    Number is clipped to num_vehicles_total. -1: no minimum. 0: create none

    const unsigned int num_charging_stations   = 3;

    double             time_sim                = 0.0; // seconds
    double             time_sim_end            = simulation_run_time_hrs * seconds_per_hour; // seconds         

    const bool         write_output_to_file    = true;
    const std::string  output_file_name        = "output.txt";
    std::ofstream      output;

    StatsAccumulator stats;

    // Construct fleet
    Fleet fleet(num_vehicles_total, num_alphas_min, num_bravos_min, num_charles_min, num_deltas_min, num_echos_min);

    // Construct aircraft terminal
    VehicleTerminal terminal(num_charging_stations);

    g_p_terminal = &terminal;

    // Loop simulation until sim end time is reached
     for (unsigned int time_step = 0; time_sim < time_sim_end; time_step++) {
        
        // Update the simulation time
        time_sim = time_step * time_delta;

        // Time step all the vehicles
        fleet.TimeStep(time_delta);
    }

    // Collect the data vehicle data and write the statistics to the output
    stats.GetFleetData(fleet);
    cout << stats << endl;

    // Write the total simulation time to stdout
    cout << "Simulation Time: " << time_sim  / (double)seconds_per_hour << " hours" << endl;
    
    // Calculate how long the simulation took to run
    auto clock_time_stop = std::chrono::system_clock::now();
    auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(clock_time_stop - clock_time_start);
    cout << "Execution time:  " << milliseconds.count() << " ms" <<endl;

    // Write the same 
    if(write_output_to_file == true)
    {
        output.open(output_file_name);

        output << stats << endl;
        output << "Simulation Time: " << time_sim  / (double)seconds_per_hour << " hours" << endl;
        output << "Execution time:  " << milliseconds.count() << " ms" <<endl;

        output.close();
    }

    return EXIT_SUCCESS;
}