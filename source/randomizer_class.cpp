// System Includes
#include <random>

// Project Imcludes
#include "randomizer_class.h"

Randomizer::Randomizer(unsigned int seed): seed_(seed) {}

// TODO use template for GetRandomNumber

double Randomizer::GetRandomNumber(double min_value, double max_value) {

    // TODO use seed

    // Local Variables
    std::random_device device;
    std::default_random_engine random_engine(device());
    std::uniform_real_distribution<double> randomizer(min_value, max_value);

    return randomizer(random_engine);
}

int Randomizer::GetRandomNumber(int min_value, int max_value)
{
    // TODO use seed

    // Local Variables
    std::random_device device;
    std::default_random_engine random_engine(device());
    std::uniform_int_distribution<unsigned int> randomizer(min_value, max_value);

    return randomizer(random_engine);
}