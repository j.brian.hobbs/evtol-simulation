// System Includes
#include <cmath>
#include <assert.h>

// Project Includes
#include "battery_class.h"

Battery::Battery(double battery_capacity_max_kwh,
                 double time_to_charge_max_h):
                 epsilson_(1.0e-10)
 {

    unsigned int seconds_per_hour = 3600; // TODO create a constants file

    // Initialize the battery parameters
    battery_capacity_max_   = battery_capacity_max_kwh;
    battery_capacity_       = battery_capacity_max_;

    time_to_charge_max_     = time_to_charge_max_h;

    assert(time_to_charge_max_ * seconds_per_hour != 0);
    charge_rate_per_second_ = battery_capacity_ / (time_to_charge_max_ * seconds_per_hour);

    is_charging_            = false;
}


double Battery::GetCapacityMax() const {
    return battery_capacity_max_;
}

double Battery::GetTimeToChargeMax() const {
    return time_to_charge_max_;
}

double Battery::Charge(double time_s) {

    // local variables    
    double amount_charged = 0.0;

    // Set the charging flag
    is_charging_ = true;

    // Check if the battery is already fully charged
    if(battery_capacity_ == battery_capacity_max_) {

        // Battery is fully charged
        is_charging_ = false;
    }
    else {

        // Get the the amount of energy that will be charged based on the charge rate of the battery
        amount_charged = (charge_rate_per_second_ * time_s);

        // Increase the battery energy
        battery_capacity_ += amount_charged; 

        // Check if the battery is now fully charged
        if (battery_capacity_ >= battery_capacity_max_ - epsilson_) {

            // Clip the energy to the max capacity of the battery
            amount_charged    = battery_capacity_max_ - battery_capacity_;
            battery_capacity_ = battery_capacity_max_;

            // Battery is fully charged
            is_charging_ = false;
        }
    }

    // Return the actual amount of energy added to the battery
    return amount_charged;
}

double Battery::Discharge(double energy_kwh) {

    // local variables
    double amount_discharged = energy_kwh;

    // Set the charging flag
    is_charging_ = false;

    // Clip the energy to the amount dischaged in the battery
    if (battery_capacity_ - amount_discharged <= epsilson_) {

        amount_discharged = battery_capacity_;
        battery_capacity_ = 0.0;
    }
    else {
        // Decrease the battery based
        battery_capacity_ -= amount_discharged;
    }

    // Return the actual amount of energy removed from the battery
    return amount_discharged;
}

bool Battery::IsFullyCharged() const {
    bool isFullyCharged = false;

    if(battery_capacity_ == battery_capacity_max_) {
        isFullyCharged = true;
    }

    return isFullyCharged;
}

bool Battery::IsCharging() const {
    return is_charging_;
}

double Battery::GetCapacity() const {
    return battery_capacity_;
}