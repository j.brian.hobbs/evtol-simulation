// System Includes
#include <string>

// Project Includes
#include "stats_accumulator_class.h"

// Namespace specifications
using std::string;
using std::endl;
using std::map;
using std::vector;

StatsAccumulator::StatsAccumulator() {
}

void StatsAccumulator::GetFleetData(const Fleet& fleet) {
    
    // Local Variables
    vector<VehicleData> alpha_data;
    vector<VehicleData> bravo_data;
    vector<VehicleData> charlie_data;
    vector<VehicleData> delta_data;
    vector<VehicleData> echo_data;

    // Create a vector for each set of vehicle type data
    vector<vector<VehicleData>* > fleet_data{&alpha_data, &bravo_data, &charlie_data, &delta_data, &echo_data};

    // Get the data for each vehicle type
    alpha_data   = fleet.GetData(VehicleType::alpha);
    bravo_data   = fleet.GetData(VehicleType::bravo);
    charlie_data = fleet.GetData(VehicleType::charlie);
    delta_data   = fleet.GetData(VehicleType::delta);
    echo_data    = fleet.GetData(VehicleType::echo);

    // Add the data to the statistics
     for(uint32_t vehicle_type_index = 0; vehicle_type_index < (uint32_t)VehicleType::size; ++vehicle_type_index) {
        for(size_t vehicle_data_index = 0; vehicle_data_index < fleet_data.at(vehicle_type_index)->size(); ++vehicle_data_index) {
            AddVehicleData((VehicleType)vehicle_type_index, fleet_data.at(vehicle_type_index)->at(vehicle_data_index));
        }
    }
}

size_t StatsAccumulator::AddFlightData(VehicleType vehicle_type, FlightData flight_data) {
    // Add the new flight statistics to the statistics accumulator
    data_[vehicle_type].flights_.push_back(flight_data);

    return data_[vehicle_type].flights_.size();
}

size_t StatsAccumulator::AddChargeData(VehicleType vehicle_type, ChargingData charge_data) {
    // Add the new charging statistics to the statistics accumulator
    data_[vehicle_type].charges_.push_back(charge_data);

    return data_[vehicle_type].charges_.size();
}

size_t StatsAccumulator::AddVehicleData(VehicleType vehicle_type, VehicleData vehicle_data) {
    // Local Variables
    size_t number_of_stats = 0; 
 

    // Add the vehicle flight statistics to the statistics accumulator
    for(size_t i = 0; i < vehicle_data.flights_.size(); ++i){
        number_of_stats += AddFlightData(vehicle_type, vehicle_data.flights_[i]);
    }

    // Add the vehicle charging statistics to the statistics accumulator
    for(size_t i = 0; i < vehicle_data.charges_.size(); ++i){
        number_of_stats +=  AddChargeData(vehicle_type, vehicle_data.charges_[i]);
    }   

    return number_of_stats;
}

size_t StatsAccumulator::GetFlightsCount(VehicleType vehicle_type) {
    return data_[vehicle_type].flights_.size();
}

size_t StatsAccumulator::GetChargesCount(VehicleType vehicle_type) {
    return data_[vehicle_type].charges_.size();
}

double StatsAccumulator::GetAvgDistTraveled(VehicleType vehicle_type) {
    // Local variables
    double dist_traveled_total = 0.0;
    double avg_dist_traveled   = 0.0;

    // Calculate the total distance flown  
    for(size_t i = 0; i < data_[vehicle_type].flights_.size(); ++i){
        dist_traveled_total += data_[vehicle_type].flights_.at(i).dist_traveled_;
    }

    // Calculate the average distance traveled
    if(data_[vehicle_type].flights_.size() != 0){
        avg_dist_traveled = dist_traveled_total / data_[vehicle_type].flights_.size();
    }

    return avg_dist_traveled;
}

double StatsAccumulator::GetAvgTimeFlight(VehicleType vehicle_type) {
    // Local variables
    double time_flight_total     = 0.0;
    double avg_time_flight_total = 0.0;

    // Calculate the total time inair  
    for(size_t i = 0; i < data_[vehicle_type].flights_.size(); ++i){
        time_flight_total += data_[vehicle_type].flights_.at(i).time_inair_;
    }

    // Calculate the average time charging
    if(data_[vehicle_type].flights_.size() != 0){
        avg_time_flight_total =  time_flight_total / data_[vehicle_type].flights_.size();
    }

    return avg_time_flight_total;
}

double StatsAccumulator::GetAvgTimeCharging(VehicleType vehicle_type) {
    // Local variables
    double time_charging_total     = 0.0;
    double avg_time_charging_total = 0.0;

    // Calculate the total time charging  
    for(size_t i = 0; i < data_[vehicle_type].charges_.size(); ++i){
        time_charging_total += data_[vehicle_type].charges_.at(i).time_charging_;
    }

    // Calculate the average time charging
    if(data_[vehicle_type].charges_.size() != 0){
        avg_time_charging_total =  time_charging_total / data_[vehicle_type].charges_.size();
    }

    return avg_time_charging_total;
}

uint32_t StatsAccumulator::GetFaultsCount(VehicleType vehicle_type) {
    // Local variables
    uint32_t faults_count_total = 0;

    // Calculate the total distance flown  
    for(size_t i = 0; i < data_[vehicle_type].flights_.size(); ++i){
        faults_count_total += data_[vehicle_type].flights_.at(i).faults_count_;
    }

    return faults_count_total;
}

double StatsAccumulator::GetPassengerMiles(VehicleType vehicle_type) {
    // Local variables
    double passenger_miles_total = 0.0;

    // Calculate the total distance flown  
    for(uint32_t i = 0; i < data_[vehicle_type].flights_.size(); ++i){
        passenger_miles_total += data_[vehicle_type].flights_.at(i).dist_passengers_;
    }

    return passenger_miles_total;
}

ostream& StatsAccumulator::PrintFlightData(ostream&    out,
                                            VehicleType vehicle_type,
                                            size_t      flight_number,
                                            bool        pretty_print) {
    // Local variables
    string flight_number_header;
    string time_inair_header;
    string time_charging_header;
    string dist_traveled_header;
    string dist_passengers_header;
    string faults_count_header;
    string passenger_count_header;
    string comma = ",";
    string newline;

    size_t index_number = flight_number - 1;

    // Get the last flight data if the flight number specifed is 0
    if(flight_number == 0)
    {
        index_number = data_[vehicle_type].flights_.size() - 1;
    }

    // Use line headers if pretty print selected
    if (pretty_print) {
        flight_number_header           = "Flight number:    ";
        time_inair_header              = "Time inair:       ";
        dist_traveled_header           = "Dist. traveled:   ";
        dist_passengers_header         = "Dist. passengers: ";
        faults_count_header            = "Faults count:     ";
        passenger_count_header         = "Passengers count: ";
        comma                          = "";
        newline                        = "\n";
    }

    try {
        out << flight_number_header   << flight_number                                                  << comma << newline;

        out << time_inair_header      << data_[vehicle_type].flights_.at(index_number).time_inair_      << comma << newline;

        out << dist_traveled_header   << data_[vehicle_type].flights_.at(index_number).dist_traveled_   << comma << newline;
        out << dist_passengers_header << data_[vehicle_type].flights_.at(index_number).dist_passengers_ << comma << newline;

        out << faults_count_header    << data_[vehicle_type].flights_.at(index_number).faults_count_    << comma << newline;
        out << passenger_count_header << data_[vehicle_type].flights_.at(index_number).passenger_count_          << newline;
    }

    catch (const std::out_of_range & ex) {
        out << "\nFlight number is invalid:" << " out_of_range Exception Caught ::" << ex.what() << endl;
    }

    return out;
}

ostream& StatsAccumulator::PrintChargeData(ostream&    out,
                                            VehicleType vehicle_type,
                                            size_t      charge_number,
                                            bool        pretty_print) {
    // Local variables
    string charge_number_header;
    string time_charging_header;
    string comma = ",";
    string newline;

    size_t index_number = charge_number - 1;

    // Get the last charging data set if the flight number specifed is 0
    if(charge_number == 0)
    {
        index_number = data_[vehicle_type].charges_.size() - 1;
    }

    // Use line headers if pretty print selected
    if (pretty_print) {
        charge_number_header = "Charging session number:    ";
        time_charging_header = "Time charging:              ";
        comma                = "";
        newline              = "\n";
    }

    try {
        out << charge_number_header   << charge_number                                               << comma << newline;
        out << time_charging_header << data_[vehicle_type].charges_.at(index_number).time_charging_  << newline;
    }

    catch (const std::out_of_range & ex) {
        out << "Charging session number is invalid:" << " out_of_range Exception Caught ::" << ex.what() << endl;
    }

    return out;
}

ostream& StatsAccumulator::PrintFlightDataHeader(ostream& out) {

    out << "flight_number,"
        << "time_inair,"
        << "dist_traveled,"
        << "dist_passengers,"
        << "faults_count,"
        << "passenger_count";

    return out;
}


ostream& StatsAccumulator::PrintChargeDataHeader(ostream& out) {

    out << "time_charging";

    return out;
}

ostream& operator<<(ostream& out, StatsAccumulator& stats_accumulator) {
    // Local variables
    vector<string>  vehicle_type_titles       = {"Alpha:", "Bravo:", "Charlie:", "Delta:", "Echo:"};

    // Save the original precision
    std::streamsize original_output_precision = out.precision();

    // Set the precision of the output
    out.precision(10);
   
    // TODO Write the number of vehicles of each vehicle type

    // Write the stats per vehicle type
    for (size_t i = 0; i < vehicle_type_titles.size(); ++i){
        out << vehicle_type_titles[i] << endl;
        out << "\tNumber of flights:      " << stats_accumulator.GetFlightsCount(VehicleType(i))    << endl;
        out << "\tNumber of charges:      " << stats_accumulator.GetChargesCount(VehicleType(i))    << endl;
        out << "\tAvg. flight time:       " << stats_accumulator.GetAvgTimeFlight(VehicleType(i))   << endl; 
        out << "\tAvg. distance traveled: " << stats_accumulator.GetAvgDistTraveled(VehicleType(i)) << endl;
        out << "\tAvg. time charging:     " << stats_accumulator.GetAvgTimeCharging(VehicleType(i)) << endl;
        out << "\tNumber of faults:       " << stats_accumulator.GetFaultsCount(VehicleType(i))     << endl;
        out << "\tPassenger miles:        " << stats_accumulator.GetPassengerMiles(VehicleType(i))  << endl;
    }

    // Restore the original output precision
    out.precision(original_output_precision);
    

    return out;
}