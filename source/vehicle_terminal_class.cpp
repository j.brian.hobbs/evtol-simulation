// System Includes
#include <algorithm>

// Project Includes
#include "vehicle_terminal_class.h"

VehicleTerminal::VehicleTerminal(uint8_t num_charging_stations)
{
    // Setup the charging stations with no vehicles connected to them
    charging_stations_.resize(num_charging_stations);

    for(size_t charging_station_index = 0; charging_station_index < charging_stations_.size(); ++charging_station_index) {
        charging_stations_.at(charging_station_index) = NULL;
    }
}

void VehicleTerminal::ConnectToAvailableChargingStation() {

    for(size_t charging_station_index = 0; charging_station_index < charging_stations_.size(); ++charging_station_index) {

        // Check if there is a free charging station
        if(charging_stations_.at(charging_station_index) == NULL) {

            for(size_t landing_pad_index = 0; landing_pad_index < landing_pads_.size(); ++landing_pad_index) {
   
                // Connect the vehicle waiting the longest to the charging station
                if(landing_pads_.size() > landing_pad_index && landing_pads_.at(landing_pad_index)->GetConnectedToCharger() == false)
                {
                    charging_stations_.at(charging_station_index) = landing_pads_.at(landing_pad_index);
                    landing_pads_.at(landing_pad_index)->ConnectToCharger();
                    return;
                }
            }
        }
    }
 }

void VehicleTerminal::DisconnectFromChargingStation(Vehicle* p_vehicle) {

   // Make sure with a valid pointer
    if(p_vehicle != NULL)
    {
        for(size_t charging_station_index = 0; charging_station_index < charging_stations_.size(); ++charging_station_index) {

            // Check if the vehicle is on a charging station
            if(charging_stations_.at(charging_station_index) == p_vehicle) {

                // Disconnect the vehicle to the charging station
                charging_stations_.at(charging_station_index) = NULL;

                // Connect the next waiting vehicle to the charging staion                
                ConnectToAvailableChargingStation();

                p_vehicle->DisconnectFromCharger();
                break;
            }
        }
    }
}

bool VehicleTerminal::AddToLandingPad(Vehicle* p_vehicle) {
    // Local variables
    bool return_value = EXIT_FAILURE;

    if(p_vehicle != NULL) {

        // Make sure the vehicle is not already on a landing pada
        if(std::find(landing_pads_.begin(), landing_pads_.end(), p_vehicle) == landing_pads_.end())
        {
            // Add the vehicle to a landing pad
            landing_pads_.push_back(p_vehicle);
            return_value = EXIT_SUCCESS;

            // Connect vehicles to charging stations
            ConnectToAvailableChargingStation(); 
        }
    }

    return return_value;
}  

bool VehicleTerminal::RemoveFromLandingPad(Vehicle* p_vehicle) {
    // Local variables
    bool return_value = EXIT_FAILURE;
    vector<Vehicle*>::const_iterator itr;

    if(p_vehicle != NULL) {

        // Look for the vehicle on a landing pad
        itr = std::find(landing_pads_.begin(), landing_pads_.end(), p_vehicle);

        // Check if the vehicle is on a landing pad
        if(itr != landing_pads_.end())
        {
            // Remove vehicles from charging stations
            DisconnectFromChargingStation(p_vehicle);

            // Remove the vehicle from it's landing pad
            landing_pads_.erase(itr);
            return_value = EXIT_SUCCESS;

        }
    }

    return return_value;
}